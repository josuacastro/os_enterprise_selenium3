package selenium.test;
 
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.edge.EdgeDriver;

import org.testng.Assert;

 public class TestSe{
	
		private WebDriver objWebDriver  = null;		
		private String  firefox = "C:\\selenium\\geckodriver-v0.24.0-win64\\geckodriver.exe";
		private String  chrome  = "C:\\selenium\\chrome_2\\chromedriver.exe"; 
		@Test
		public void prueba_firefox(){
			   System.out.println( "empieza prueba firefox " );
			   
			   try {
				   System.setProperty( "webdriver.gecko.driver", this.firefox ); 
			       this.objWebDriver = new  FirefoxDriver();
			       objWebDriver.get("http://192.168.1.47:8080/petclinic.war");
			        if (!objWebDriver.getTitle().equals("Enterprise"))
			          throw new AssertionError("el titulo no coincide");
			        System.out.println("Esta correcto, Eres lo maximo!!");
			      }			   catch( Exception e ){
			          e.printStackTrace();
			   } 
		       finally{
		    	       if( this.objWebDriver != null ) {
		    	           this.objWebDriver.close(); 
		    	       }
		    	       
			           System.out.println( "finalizo firefox" );
		       }
			   
		}	
		
		@Test
		public void prueba_chrome(){
			   System.out.println( "empieza prueba chrome" );
			   
			   try {
				   System.setProperty( "webdriver.chrome.driver", this.chrome ); 
			       this.objWebDriver = new  ChromeDriver();
			       objWebDriver.get("http://192.168.1.47:8080/petclinic.war");
			        if (!objWebDriver.getTitle().equals("Enterprise"))
			          throw new AssertionError("el titulo no coincide");
			        System.out.println("Esta correcto, Eres lo maximo!!");
			      }			   catch( Exception e ){
			          e.printStackTrace();
			   } 
		       finally{
		    	       if( this.objWebDriver != null ) {
		    	           this.objWebDriver.close(); 
		    	       }
		    	       
			           System.out.println( "finalizo chrome" );
		       }
			   
		}
	
	
 }
 
